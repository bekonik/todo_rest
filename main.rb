# encoding: UTF-8
require 'json'
require 'sinatra'
require 'data_mapper'
require 'dm-migrations'
require 'bundler/setup'
require 'yaml'

# configure :development do
#   DataMapper::Logger.new($stdout, :debug)
#   DataMapper.setup(
#     :default,
#     ENV['DATABASE_URL'] || 'postgres://localhost:5432/todo'
#   )
# end 

dbconfig = YAML.load(ERB.new(File.read("config/database.yml")).result)
RACK_ENV ||= ENV["RACK_ENV"] || "development"
DataMapper.setup(:default, dbconfig[RACK_ENV])

# use Rack:Auth::Basic, "Restricted area" do |username, password|
# 	username = 'admin' and password = 'admin'
# end

require './models/init'
require './helpers/init'
require './routes/init'

DataMapper.finalize 